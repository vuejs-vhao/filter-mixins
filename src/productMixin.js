export const productMixin = {
  data() {
    return {
      products: [
        "Iphone",
        "Samsung",
        "HTC",
        "Nokia",
        "Noway",
        "Bphone",
        "Oppo",
        "Bla Bla"
      ],
      filterProduct: ""
    };
  },
  computed: {
    filteredProducts() {
      return this.products.filter(prd => prd.match(this.filterProduct));
    }
  },
  created() {
    console.log("Created from mixins");
  }
};
